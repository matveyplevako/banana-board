import React from "react";
import { shallow } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import * as serverListData from "../../../../../__data__/selectors/loadServerList";
import * as serverDetail from "../../../../../__data__/selectors/loadServerDetail";

import Main from "../Main";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: () => jest.fn(),
}));

jest.mock("react-router-dom", () => ({
  useParams: jest.fn(),
}));

jest.spyOn(React, "useEffect").mockImplementation((func) => func());

describe("<Main />", () => {
  it("Main получил список серверов, но serverId не задан", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case serverListData.data:
        return true;
      case serverDetail.serverId:
        return "";
      }
    });
    (useParams as any).mockImplementation(() => "");
    const wrapper = shallow(<Main />);
    expect(wrapper).toMatchSnapshot();
  });

  it("Main получил serverId, но не загрузил данные", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case serverListData.data:
        return ["Server1", "Server2"];
      case serverDetail.serverId:
        return "";
      }
    });
    (useParams as any).mockReturnValue({ serverId: "Server1" });
    const wrapper = shallow(<Main />);
    expect(wrapper).toMatchSnapshot();
  });

  it("Main запрос несуществующей страницы", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case serverListData.data:
        return ["Server1", "Server2"];
      case serverDetail.error:
        return "Not Found";
      }
    });
    (useParams as any).mockReturnValue({ serverId: "Server1" });
    const wrapper = shallow(<Main />);
    expect(wrapper).toMatchSnapshot();
  });
});
