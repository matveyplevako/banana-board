import React from "react";
import i18next from "i18next";
import { mount } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";
import { BrowserRouter } from "react-router-dom";

import Navbar from "../Navbar";

jest.mock("react-redux", () => ({
  useDispatch: () => jest.fn(),
}));

jest.mock("i18next", () => ({
  changeLanguage: jest
    .fn()
    .mockImplementationOnce(() => "ru")
    .mockImplementationOnce(() => "en"),
}));

describe("<Navbar />", () => {
  it("Navbar смена языка", () => {
    const wrapper = mount(
      <BrowserRouter>
        <Navbar path={"Server1"} />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.exists("#ENG_svg")).toBeTruthy();

    wrapper.find("#languageSwitch").simulate("click");

    wrapper.update();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.exists("#RU_svg")).toBeTruthy();

    i18next.language = "ru";
    wrapper.find("#languageSwitch").simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("Navbar уведомления", () => {
    const wrapper = mount(
      <BrowserRouter>
        <Navbar path={"Server1"} />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
    wrapper.find("#notifications").simulate("click");

    wrapper.update();
    expect(wrapper).toMatchSnapshot();

    wrapper.find(".Close").first().simulate("click");
    wrapper.find(".Close").last().simulate("click");
    wrapper.find(".Close").first().simulate("click");
    wrapper.find(".Close").first().simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("Navbar выход", () => {
    Storage.prototype.removeItem = jest.fn();
    const wrapper = mount(
      <BrowserRouter>
        <Navbar path={"Server1"} />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
    wrapper.find("#showMenu").simulate("click");

    wrapper.update();
    expect(wrapper).toMatchSnapshot();

    wrapper.find("#exit").simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });
});
