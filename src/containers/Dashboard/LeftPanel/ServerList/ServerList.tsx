import React from "react";
import style from "./style.css";

import { Server } from "./Server";
import { Loading } from "../../../../components/Loading";
import { useSelector } from "react-redux";
import * as serverListData from "../../../../__data__/selectors/loadServerList";
import * as serverDetail from "../../../../__data__/selectors/loadServerDetail";

const ServerList = () => {
  const data = useSelector(serverListData.data);
  const loading = useSelector(serverListData.loading);
  const currentServerId = useSelector(serverDetail.serverId);

  let listItems = [
    <div className={style.Loading} key={"Loading"}>
      <Loading />
    </div>,
  ];

  if (!loading) {
    listItems = data.map((server) => (
      <Server
        key={server}
        serverId={server}
        active={server === currentServerId}
      />
    ));
  }

  return <div className={style.ServerList}>{listItems}</div>;
};

export default ServerList;
