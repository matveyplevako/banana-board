import { createSelector } from "reselect";

import { AppStore } from "..";
import { LoginState } from "../slices/login";

const rootSelector = createSelector<AppStore, AppStore, LoginState>(
  (state) => state,
  (state) => state.login
);

export const loading = createSelector(rootSelector, (state) => state.loading);
export const data = createSelector(rootSelector, (state) => state.data);
export const error = createSelector(rootSelector, (state) => state.error);
export const validationError = createSelector(
  rootSelector,
  (state) => state.validationError
);

const formData = createSelector(rootSelector, (state) => state.form);

export const password = createSelector(formData, (state) => state.password);
export const login = createSelector(formData, (formData) => formData.login);
