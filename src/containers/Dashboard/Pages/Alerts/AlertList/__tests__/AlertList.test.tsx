import React from "react";
import { mount } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";
import { useSelector } from "react-redux";

import * as alerts from "../../../../../../__data__/selectors/alerts";

import AlertList from "../AlertList";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: () => jest.fn(),
}));

jest.spyOn(React, "useEffect").mockImplementation((func) => func());

describe("<AlertList />", () => {
  it("AlertList загрузка", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case alerts.data:
        return false;
      case alerts.loading:
        return true;
      }
    });
    const wrapper = mount(<AlertList />);
    expect(wrapper).toMatchSnapshot();
  });

  it("AlertList с данными", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case alerts.loading:
        return false;
      case alerts.data:
        return [
          {
            id: 3,
            name: "No space left",
            server: "Backup Server",
            resource: "MEM",
            value: ">80%",
            active: true,
          },
        ];
      }
    });
    const wrapper = mount(<AlertList />);
    expect(wrapper).toMatchSnapshot();

    wrapper.find(".RoundedBorders").first().simulate("click");
    expect(wrapper).toMatchSnapshot();

    wrapper.find(".RoundedBorders").first().simulate("click");
    expect(wrapper).toMatchSnapshot();

    wrapper.find(".RoundedBorders").last().simulate("click");
    expect(wrapper).toMatchSnapshot();
  });
});
