describe("Logout test", () => {
    it("Visits the banana board and log out than visit again", () => {
        cy.visit("/auth");

        cy.url().should("include", "/auth");

        cy.get("#login")
            .type("saymyname@yandex.ru")
            .should("have.value", "saymyname@yandex.ru");

        cy.get("#password").type("qwerty123").should("have.value", "qwerty123");

        cy.get("#signIn").click();


        cy.get('#showMenu').click();
        cy.contains('Role').should('exist');
        cy.get('#showMenu').click();
        cy.get('#showMenu').click();
        cy.get('#exit').click();

        cy.get("#welcomeButton").click();

        cy.url().should("include", "/auth");

        cy.get("#login")
            .type("saymyname@yandex.ru")
            .should("have.value", "saymyname@yandex.ru");

        cy.get("#password").type("qwerty123").should("have.value", "qwerty123");

        cy.get("#signIn").click();
    });
});
