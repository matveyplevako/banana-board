import axios from "axios";
import { getConfigValue } from "@ijl/cli";

const baseApiUrl = getConfigValue("banana-board.api");
export const bananaAxios = axios.create({
  baseURL: baseApiUrl,
  headers: {
    "Content-Type": "application/json;charset=utf-8",
  },
});
