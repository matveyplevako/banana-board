import {
  reducer,
  handleSubmit,
  handleSuccess,
  failure,
} from "../loadServerList";
import { submitRequest } from "../../actions/loadServerList";

import { bananaAxios } from "../../../utils";

import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { describe, it, expect, jest } from "@jest/globals";

jest.mock("../../../utils/axios.ts", () => ({
  bananaAxios: jest.fn(),
}));

const mockStore = configureMockStore([thunk]);

describe("loadServerList", () => {
  describe("loadServerList slices", () => {
    it("loadServerList запрос данных", async () => {
      const nextState = reducer(undefined, handleSubmit());

      expect(nextState).toEqual({
        loading: true,
        data: null,
        error: null,
      });
    });

    it("loadServerList получены данные", async () => {
      const nextState = reducer(undefined, handleSuccess(["payload"]));

      expect(nextState).toEqual({
        loading: false,
        data: ["payload"],
        error: null,
      });
    });

    it("loadServerList ошибка при запросе", () => {
      const nextState = reducer(undefined, failure("error message"));

      expect(nextState).toEqual({
        loading: false,
        data: null,
        error: "error message",
      });
    });
  });

  describe("loadServerList actions", () => {
    it("loadServerList запрос данных", async () => {
      const success = ["Server1", "Server2"];
      (bananaAxios as any).mockReturnValue({
        data: { serverList: success },
      });
      const store = mockStore();
      await store.dispatch(submitRequest());

      const expectedActions = [handleSubmit(), handleSuccess(success)];
      expect(store.getActions()).toEqual(expectedActions);
    });

    it("loadServerList ошибка", async () => {
      (bananaAxios as any).mockImplementation(() => {
        throw "error";
      });
      const store = mockStore();
      await store.dispatch(submitRequest());

      const expectedActions = [handleSubmit(), failure("error")];
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
