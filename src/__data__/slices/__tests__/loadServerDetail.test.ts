import {
  reducer,
  handleSubmit,
  handleSuccess,
  changeServerId,
  clear,
  failure,
} from "../loadServerDetail";
import { submitRequest } from "../../actions/loadServerDetail";

import { bananaAxios } from "../../../utils";

import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { describe, it, expect, jest } from "@jest/globals";

jest.mock("../../../utils/axios.ts", () => ({
  bananaAxios: jest.fn(),
}));

const mockStore = configureMockStore([thunk]);

describe("loadServerDetail", () => {
  describe("loadServerDetail slices", () => {
    it("loadServerDetail запрос данных", async () => {
      const nextState = reducer(undefined, handleSubmit());

      expect(nextState).toEqual({
        serverId: null,
        loading: true,
        data: null,
        error: null,
      });
    });

    it("loadServerDetail получены данные", async () => {
      const nextState = reducer(undefined, handleSuccess(["payload"]));

      expect(nextState).toEqual({
        loading: false,
        data: ["payload"],
        error: null,
        serverId: null,
      });
    });

    it("loadServerDetail смена serverId", async () => {
      const nextState = reducer(undefined, changeServerId("Server1"));

      expect(nextState).toEqual({
        loading: true,
        data: null,
        serverId: "Server1",
        error: null,
      });
    });

    it("loadServerDetail сброс текущих данных", async () => {
      const nextState = reducer(undefined, clear());

      expect(nextState).toEqual({
        loading: true,
        data: null,
        serverId: null,
        error: null,
      });
    });

    it("loadServerDetail ошибка при запросе", () => {
      const nextState = reducer(undefined, failure("error message"));

      expect(nextState).toEqual({
        loading: false,
        serverId: null,
        data: null,
        error: "error message",
      });
    });
  });

  describe("loadServerDetail actions", () => {
    it("loadServerDetail запрос данных", async () => {
      const serverId = "Server1";
      const success = ["data"];
      (bananaAxios as any).mockReturnValue({
        data: success,
      });
      const store = mockStore();
      await store.dispatch(submitRequest(serverId));

      const expectedActions = [
        changeServerId(serverId),
        handleSubmit(),
        handleSuccess(success),
      ];
      expect(store.getActions()).toEqual(expectedActions);
    });

    it("loadServerDetail ошибка", async () => {
      const serverId = "Server1";
      const error = { response: { statusText: "Not Found" } };
      (bananaAxios as any).mockImplementation(() => {
        throw error;
      });
      const store = mockStore();
      await store.dispatch(submitRequest(serverId));

      const expectedActions = [
        changeServerId(serverId),
        handleSubmit(),
        failure(error["response"]["statusText"]),
      ];
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
