import { createSelector } from "reselect";

import { AppStore } from "..";
import { Alerts } from "../slices/alerts";

const rootSelector = createSelector<AppStore, AppStore, Alerts>(
  (state) => state,
  (state) => state.alerts
);

export const loading = createSelector(rootSelector, (state) => state.loading);
export const data = createSelector(rootSelector, (state) => state.data);
export const error = createSelector(rootSelector, (state) => state.error);
