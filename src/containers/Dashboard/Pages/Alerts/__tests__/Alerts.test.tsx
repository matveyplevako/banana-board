import React from "react";
import { shallow } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";

import Alerts from "../Alerts";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: () => jest.fn(),
}));

jest.spyOn(React, "useEffect").mockImplementation((func) => func());

describe("<Alerts />", () => {
  it("Alerts", () => {
    const wrapper = shallow(<Alerts />);
    expect(wrapper).toMatchSnapshot();
  });
});
