const router = require("express").Router();

const TWO_SECONDS = 2000;
const HALF_SECOND = 500;

// const wait = (time = TWO_SECONDS) => (req, res, next) => setTimeout(next, time);
const wait = (time = HALF_SECOND) => (req, res, next) => setTimeout(next, time);

const stubs = {
  login: "success",
  serverList: "success",
};

router.post("/login", wait(), (req, res) => {
  if (req.body.login === "error" || stubs.login === "error") {
    return res.status(500).send({ message: "Закрыто на обед!" });
  }

  res.send(require(`./mocks/login/${stubs.login}`));
  // res.status(500).send({"code":2,"error":"Не верный логин или пароль"});
});

router.get("/serverList", wait(), (req, res) => {
  res.send(require(`./mocks/serverList/${stubs.serverList}`));
});

router.get("/server/:id", wait(), (req, res) => {
  try {
    res.send(require(`./mocks/server/${req.params.id}`));
  } catch (e) {
    res.status(404).send();
  }
});

router.post("/server/:id/manage", wait(), (req, res) => {
  res.send(require(`./mocks/manage/success`));
});

router.get("/alerts", wait(), (req, res) => {
  res.send(require(`./mocks/alerts/success`));
});

router.post("/alerts", wait(), (req, res) => {
  let data = req.body;
  data["id"] = Math.floor(Math.random() * 1000);
  data["active"] = true;
  res.send(data);
});

module.exports = router;
