import React from "react";
import { mount } from "enzyme";

import { describe, it, expect } from "@jest/globals";

import Loading from "../Loading";

describe("<Loading />", () => {
  it("Loading рендерится без ошибок", () => {
    const wrapper = mount(<Loading />);
    expect(wrapper).toMatchSnapshot();
  });
});
