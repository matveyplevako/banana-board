import React, { useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import style from "./style.css";

import { useTranslation } from "react-i18next";

import { Logo } from "../../components/Labels";
import { Input } from "../../components/Input";
import { SignIn } from "../../components/Buttons";

import { URLs } from "../../urls";

import {
  submitLogin,
  handleSetFormLogin as changeLoginAction,
  handleSetFormPassword as changePasswordAction,
  checkLogin,
} from "../../__data__/actions/login";
import * as selectors from "../../__data__/selectors/login";

export const Auth = () => {
  const [t] = useTranslation();

  const data = useSelector(selectors.data);
  const [needRedirect, setNeedRedirect] = useState(false);
  const [empty, setEmpty] = useState({ login: false, password: false });
  const login = useSelector(selectors.login);
  const password = useSelector(selectors.password);
  const loading = useSelector(selectors.loading);

  const dispatch = useDispatch();
  const submitLoginForm = (login, password) => {
    dispatch(submitLogin({ login, password }));
  };
  const setLogin = (value) => dispatch(changeLoginAction(value));
  const setPassword = (value) => dispatch(changePasswordAction(value));

  useEffect(() => {
    dispatch(checkLogin());
  }, []);

  useEffect(() => {
    if (data) {
      setNeedRedirect(true);
    }
  }, [data]);

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    if (!login || !password) {
      setEmpty({ login: login === "", password: password === "" });
      return;
    }

    submitLoginForm(login, password);
  }

  function handleLoginChange(event) {
    if (empty["login"]) {
      setEmpty({ ...empty, login: false });
    }
    setLogin(event.target.value);
  }

  function handlePasswordChange(event) {
    if (empty["password"]) {
      setEmpty({ ...empty, password: false });
    }
    setPassword(event.target.value);
  }

  if (needRedirect) {
    return <Redirect to={URLs.dashboard.url} />;
  }

  return (
    <div className={style.authBody}>
      <form className={style.authWrapper} onSubmit={handleSubmit}>
        <Logo />

        <Input
          onChange={handleLoginChange}
          type="text"
          placeholder={t("dashboard.auth.login")}
          className={`${style.loginMargin} ${empty["login"] ? style.Red : ""}`}
          id="login"
          disabled={loading}
        />
        <Input
          type="password"
          onChange={handlePasswordChange}
          placeholder={t("dashboard.auth.pass")}
          className={`${style.loginMargin} ${
            empty["password"] ? style.Red : ""
          }`}
          id="password"
          disabled={loading}
        />

        <div className={style.authButtonWrapper}>
          <SignIn showLoading={loading} />
        </div>

        <div className={style.authTextWrapper}>
          <p>
            {t("dashboard.auth.terms.text")}{" "}
            <a href="index.html" className={style.authTextLink}>
              {t("dashboard.auth.terms")}
            </a>
            .
          </p>
          <p>
            {t("dashboard.auth.privacy.text")}{" "}
            <a href="" className={style.authTextLink}>
              {t("dashboard.auth.privacy")}
            </a>{" "}
            {t("dashboard.auth.and")}{" "}
            <a href="" className={style.authTextLink}>
              {t("dashboard.auth.cookie")}
            </a>
            .
          </p>
        </div>
      </form>
    </div>
  );
};
