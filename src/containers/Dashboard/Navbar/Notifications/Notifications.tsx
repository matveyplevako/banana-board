import React, { useState } from "react";
import style from "./style.css";
import { Cross } from "../../../../assets";

import { useTranslation } from "react-i18next";

import PropTypes from "prop-types";

const Notifications = (props) => {
  const [t] = useTranslation();

  const initialState = [
    ["Alert1", "Server1", "10.04.2021", "00:20", "CPU>70%"],
    ["Alert2", "Server2", "10.04.2021", "07:10", "MEM>90%"],
    ["Alert3", "Backup Server", "10.04.2021", "10:30", "RAM>80%"],
    ["Alert4", "Server1", "10.04.2021", "12:40", "CPU<5%"],
  ];
  const [notificationsData, setNotificationsData] = useState(initialState);

  let notifications = notificationsData.map((data) => (
    <div className={style.Notification} key={data[0]}>
      <div className={style.AlertTitle}>
        <p>{data[0]}</p>
        <p>{data[1]}</p>
        <div style={{ color: "black" }}>
          <p>{data[2]}</p>
          <p>{data[3]}</p>
        </div>
      </div>
      <div className={style.AlertContent}>
        <p>{data[4]}</p>
      </div>
      <img
        src={Cross}
        className={style.Close}
        onClick={() => {
          setNotificationsData(
            notificationsData.filter((item) => item[0] !== data[0])
          );
        }}
      />
    </div>
  ));

  if (notificationsData.length == 0) {
    notifications = [
      <div className={style.Notification} key="No new notifications">
        {t("dashboard.navbar.notifications.no.new")}
      </div>,
    ];
  }
  return (
    <div
      className={`${style.NotificationContainer} ${
        props.showNotificationList ? "" : style.hide
      }`}
    >
      <div className={style.BoxText}>{t("dashboard.navbar.notifications")}</div>
      {notifications}
    </div>
  );
};

Notifications.propTypes = {
  showNotificationList: PropTypes.bool,
};

export default Notifications;
