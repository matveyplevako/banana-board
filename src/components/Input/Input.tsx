import React from "react";

import style from "./style.css";

const Input = (raw_props) => {
  const { className, ...props } = raw_props;
  return <input {...props} className={`${className} ${style.authForm}`} />;
};

export { Input };
