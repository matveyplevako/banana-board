import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export type ServerListState = {
  loading: boolean;
  data: string[];
  error: boolean | string;
};

const initialState: ServerListState = {
  loading: true,
  data: null,
  error: null,
};

const slice = createSlice({
  name: "server-list",
  initialState,
  reducers: {
    handleSubmit(state) {
      state.loading = true;
    },
    handleSuccess(state, action) {
      state.data = action.payload;
      state.loading = false;
    },
    failure(state, action: PayloadAction<string | boolean>) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const { handleSubmit, handleSuccess, failure } = slice.actions;

export const reducer = slice.reducer;
