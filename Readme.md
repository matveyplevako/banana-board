# 🍌 Banana board

Dashboard for managing cloud resources ☁️

## 🪚 MVP-1

1. welcome page
2. authorization page
3. dashboard page

- display resources utilization
- managing resources
- display notifications

4. settings page

- user preferences
- choice of dashboard elements to be shown

5. alerts page

- display all alerts
- create alerts

### Figma

[figma](https://www.figma.com/file/iVBJsWaHYakKwsr9ccerwy/BananaBoard?node-id=43%3A34)

## ⚙️ Install

`npm install`

## 📀 Run

`npm start`
