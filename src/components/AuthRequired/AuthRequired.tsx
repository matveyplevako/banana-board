import React from "react";

import { Route, Redirect } from "react-router-dom";
import { URLs } from "../../urls";

import PropTypes from "prop-types";

const AuthRequired = (props) => {
  return (
    <Route path={props.path}>
      {window.localStorage.getItem("banana:token") ? (
        props.children
      ) : (
        <Redirect to={URLs.auth.url} />
      )}
    </Route>
  );
};

AuthRequired.propTypes = {
  path: PropTypes.string,
  children: PropTypes.node,
};

export default AuthRequired;
