import React from "react";
import { shallow } from "enzyme";

import { describe, it, expect } from "@jest/globals";

import { SignIn } from "../SignIn";

describe("<SignIn />", () => {
  it("SignIn рендерится без ошибок", () => {
    const wrapper = shallow(
      <SignIn />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("SignIn показывает загрузку", () => {
    const wrapper = shallow(
      <SignIn showLoading={true} />
    );

    expect(wrapper).toMatchSnapshot();
  });
});
