import { createSlice, PayloadAction } from "@reduxjs/toolkit";

type LoginForm = {
  password: string;
  login: string;
};

export type LoginState = {
  loading: boolean;
  data: { token: string };
  error: boolean | string;
  validationError: boolean | string;
  form: LoginForm;
};

const initialState: LoginState = {
  loading: false,
  data: null,
  error: null,
  validationError: false,
  form: {
    password: "",
    login: "",
  },
};

const slice = createSlice({
  name: "login",
  initialState,
  reducers: {
    handleSubmit(state) {
      state.loading = true;
      state.error = null;
    },
    handleSuccess(state, action) {
      state.data = action.payload;
      state.loading = false;
    },
    handleSetValidationError(state, action) {
      state.validationError = action.payload;
    },
    handleSetFormLogin(state, action) {
      state.error = null;
      state.validationError = false;
      state.form = {
        ...state.form,
        login: action.payload,
      };
    },
    handleSetFormPassword(state, action) {
      state.error = null;
      state.validationError = false;
      state.form = {
        ...state.form,
        password: action.payload,
      };
    },
    logout(state) {
      state.data = null;
      state.form = { login: "", password: "" };
      state.loading = false;
    },
    failure(state, action: PayloadAction<string | boolean>) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const {
  logout,
  handleSubmit,
  handleSuccess,
  handleSetValidationError,
  handleSetFormLogin,
  handleSetFormPassword,
  failure,
} = slice.actions;

export const reducer = slice.reducer;
