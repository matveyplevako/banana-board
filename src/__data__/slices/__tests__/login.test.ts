import {
  reducer,
  logout,
  handleSubmit,
  handleSuccess,
  handleSetValidationError,
  handleSetFormLogin,
  handleSetFormPassword,
  failure,
} from "../login";
import { submitLogin, checkLogin } from "../../actions/login";

import { bananaAxios } from "../../../utils";

import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { describe, it, expect, jest } from "@jest/globals";

jest.mock("../../../utils/axios.ts", () => ({
  bananaAxios: jest.fn(),
}));

const mockStore = configureMockStore([thunk]);

describe("login", () => {
  describe("login slices", () => {
    it("login ввод данных", async () => {
      const nextStateLogin = reducer(undefined, handleSetFormLogin("login"));

      expect(nextStateLogin).toEqual({
        form: {
          login: "login",
          password: "",
        },
        validationError: false,
        loading: false,
        data: null,
        error: null,
      });

      const nextStatePassword = reducer(
        undefined,
        handleSetFormPassword("password")
      );

      expect(nextStatePassword).toEqual({
        form: {
          login: "",
          password: "password",
        },
        validationError: false,
        loading: false,
        data: null,
        error: null,
      });
    });

    it("login запрос логина", async () => {
      const nextState = reducer(undefined, handleSubmit());

      expect(nextState).toEqual({
        form: {
          login: "",
          password: "",
        },
        validationError: false,
        loading: true,
        data: null,
        error: null,
      });
    });

    it("login получены данные", async () => {
      const nextState = reducer(undefined, handleSuccess(["token"]));

      expect(nextState).toEqual({
        form: {
          login: "",
          password: "",
        },
        validationError: false,
        loading: false,
        data: ["token"],
        error: null,
      });
    });
  });

  it("login logout", async () => {
    const nextState = reducer(undefined, logout());

    expect(nextState).toEqual({
      form: {
        login: "",
        password: "",
      },
      validationError: false,
      loading: false,
      data: null,
      error: null,
    });
  });

  describe("login actions", () => {
    it("login сабмит логин", async () => {
      const data = { login: "login", password: "password" };
      (bananaAxios as any).mockReturnValue({
        data: { token: "token" },
      });
      const store = mockStore();
      await store.dispatch(submitLogin(data));

      const expectedActions = [handleSubmit(), handleSuccess("token")];
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("login проверить авторизацию", async () => {
    const store = mockStore();
    Storage.prototype.getItem = () => "token";
    await store.dispatch(checkLogin());

    const expectedActions = [handleSuccess("token")];
    expect(store.getActions()).toEqual(expectedActions);
  });
});
