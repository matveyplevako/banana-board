import { Auth } from "./auth";
import { GetStarted } from "./getStarted";
import { Dashboard } from "./Dashboard";
import { Settings } from "./Dashboard/Pages/Settings";
import { Main } from "./Dashboard/Pages/Main";
import { Alerts } from "./Dashboard/Pages/Alerts";
import { Page404 } from "./Errors/Page404";

export { Auth, GetStarted, Dashboard, Settings, Alerts, Main, Page404 };
