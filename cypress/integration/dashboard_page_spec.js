describe("Dashboard servers test", () => {
    it("Visits the dashboard and get started", () => {
        cy.visit("/auth");

        cy.url().should("include", "/auth");

        cy.get("#login")
            .type("saymyname@yandex.ru")
            .should("have.value", "saymyname@yandex.ru");

        cy.get("#password").type("qwerty123").should("have.value", "qwerty123");

        cy.get("#signIn").click();


        cy.get('[href="/banana-board/dashboard/Server2"]').click();

        cy.contains("Network");
        cy.contains("Manage Server");
        cy.contains("CPU");
        cy.contains("RAM");
        cy.contains("Memory");
        cy.contains("Specs");

        cy.get('[href="/banana-board/dashboard/Backup Server"]').click();

        cy.contains("Network");
        cy.contains("Manage Server");
        cy.contains("CPU");
        cy.contains("RAM");
        cy.contains("Memory");
        cy.contains("Specs");

        cy.get('[href="/banana-board/dashboard/CI-CD server 1"]').click();

        cy.contains("Network");
        cy.contains("Manage Server");
        cy.contains("CPU");
        cy.contains("RAM");
        cy.contains("Memory");
        cy.contains("Specs");

        cy.get('[href="/banana-board/dashboard/CI-CD server 2"]').click();

        cy.contains("Network");
        cy.contains("Manage Server");
        cy.contains("CPU");
        cy.contains("RAM");
        cy.contains("Memory");
        cy.contains("Specs");

        cy.get('[title="Change language"]').click();

        cy.contains("Сеть");

        cy.contains("Включить");

        cy.contains("Включить").click();

        cy.get('[href="/banana-board/dashboard/Backup Server"]').click();

        cy.contains("Выключить");

        cy.contains("Выключить").click();

        cy.contains("настройки").click();

        cy.get("#network").click();
        cy.get("#cpu").click();

        cy.get('[href="/banana-board/dashboard/Server2"]').click();

        cy.contains("ОЗУ");
        cy.contains("Характеристики");

        cy.contains("Сеть").should("not.exist");

        cy.contains("ЦПУ").should("not.exist");

        cy.get(
            "._-_-_-_-_-_-_-src-containers-Dashboard-Navbar-Username---style__Arrow--37N"
        ).click();
        cy.get(
            "._-_-_-_-_-_-_-src-containers-Dashboard-Navbar-Username---style__RoundedBorders--1sW"
        ).click();
    });
});
