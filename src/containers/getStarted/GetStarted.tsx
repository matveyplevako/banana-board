import React from "react";
import style from "./style.css";

import { useTranslation } from "react-i18next";

import { WelcomeButton } from "../../components/Buttons";
import { Logo } from "../../components/Labels";

export const GetStarted = () => {
  const [t] = useTranslation();
  return (
    <div className={style.welcomeBody}>
      <div className={style.welcomeWrapper}>
        <Logo />
        <p className={style.welcomeText}>{t("dashboard.welcome")}</p>
        <p className={style.welcomeTextTeam}>{t("dashboard.welcome.team")}!</p>
        <a className={style.ahref} href="auth.html"></a>
        <WelcomeButton />
      </div>
    </div>
  );
};
