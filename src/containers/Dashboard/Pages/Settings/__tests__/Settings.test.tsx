import React from "react";
import { shallow } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";

import Settings from "../Settings";

jest.mock("react-redux", () => ({
  useDispatch: () => jest.fn(),
}));

describe("<Settings />", () => {
  it("Settings", () => {
    const wrapper = shallow(<Settings />);
    expect(wrapper).toMatchSnapshot();
  });
});
