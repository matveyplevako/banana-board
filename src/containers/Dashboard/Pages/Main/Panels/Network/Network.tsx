import React from "react";
import style from "./style.css";

import { useTranslation } from "react-i18next";
import { Bar } from "react-chartjs-2";

import { Box } from "../../../../../../components/Box";

import PropTypes from "prop-types";

function timeline() {
  const today = new Date();
  let hours = today.getHours();
  let minutes = Math.floor(today.getMinutes() / 10) * 10 + 10; // because we firstly substract 10 from minutes;

  return Array.from(Array(30).keys())
    .map(() => {
      if (minutes === 0) {
        minutes = 50;
        if (hours === 0) {
          hours = 23;
        } else {
          hours -= 1;
        }
      } else {
        minutes -= 10;
      }
      return `${hours}:${minutes}`;
    })
    .reverse();
}

function getChartData(data) {
  return {
    labels: timeline(),
    datasets: [
      {
        data: [...data],
        backgroundColor: "rgba(47, 128, 237, 1)",
        borderColor: "rgba(47, 128, 237, 1)",
        borderWidth: 1,
      },
    ],
  };
}

export const options = {
  cutoutPercentage: 80,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  tooltips: {
    enabled: false,
  },
};

const Network = (props) => {
  const [t] = useTranslation();
  const data = props.data;

  return (
    <Box title={t("dashboard.network")} className={style.Network}>
      <div className={style.ChartContainer}>
        <Bar data={getChartData(data)} options={options} />
      </div>
    </Box>
  );
};

Network.propTypes = {
  data: PropTypes.array,
};

export default Network;
