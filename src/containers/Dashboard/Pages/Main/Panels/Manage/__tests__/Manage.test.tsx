import React from "react";
import { shallow } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";

import { bananaAxios } from "../../../../../../../utils";
import Manage from "../Manage";

jest.mock("../../../../../../../utils/axios.ts", () => ({
  bananaAxios: jest.fn(),
}));

describe("<Manage />", () => {

  it("Manage switch off server ", () => {
    const wrapper = shallow(<Manage serverId="ServerId" data={true} />);

    wrapper.find("#switch").simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();

    // prohibit clicking while switching
    wrapper.find("#switch").simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("Manage switch on server ", () => {
    const wrapper = shallow(<Manage serverId="ServerId" data={false} />);

    wrapper.find("#switch").simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("Manage reload", () => {
    const wrapper = shallow(<Manage serverId="ServerId" data={true} />);

    wrapper.find("#reload").simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();

    wrapper.find("#reload").simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("Manage errors while loading data", () => {
    (bananaAxios as any).mockImplementation(() => {
      throw "error";
    });
    const wrapper = shallow(<Manage serverId="ServerId" data={true} />);

    wrapper.find("#switch").simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("Manage errors while reloading data", () => {
    (bananaAxios as any).mockImplementation(() => {
      throw "error";
    });
    const wrapper = shallow(<Manage serverId="ServerId" data={true} />);

    wrapper.find("#reload").simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });
});
