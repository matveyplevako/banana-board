import { createSlice } from "@reduxjs/toolkit";

export type ServerSpecs = {
  resource: string;
  type: string;
};

type ServerDetail = {
  resource: string;
  value: string | number | number[] | ServerSpecs;
};

export type ServerDetailState = {
  loading: boolean;
  data: ServerDetail;
  serverId: string;
  error: boolean | string;
};

const initialState: ServerDetailState = {
  loading: true,
  data: null,
  serverId: null,
  error: null,
};

const slice = createSlice({
  name: "server-detail",
  initialState,
  reducers: {
    handleSubmit(state) {
      state.loading = true;
    },
    handleSuccess(state, action) {
      state.data = action.payload;
      state.loading = false;
    },
    changeServerId(state, action) {
      state.serverId = action.payload;
    },
    clear(state) {
      state.loading = true;
      state.data = null;
      state.serverId = null;
    },
    failure(state, action) {
      state.loading = false;
      state.error = action.payload;
    },
    clearError(state) {
      state.error = null;
    },
  },
});

export const {
  handleSubmit,
  handleSuccess,
  changeServerId,
  clear,
  clearError,
  failure,
} = slice.actions;

export const reducer = slice.reducer;
