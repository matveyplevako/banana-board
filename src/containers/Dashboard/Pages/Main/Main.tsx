import React, { useEffect, useState } from "react";

import { Dashboard } from "../../";
import { Page404 } from "../../../Errors/Page404";
import { Panels } from "./Panels";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import * as serverListData from "../../../../__data__/selectors/loadServerList";
import * as serverDetail from "../../../../__data__/selectors/loadServerDetail";

import { submitRequest as sumbitDetailRequest } from "../../../../__data__/actions/loadServerDetail";
import { clearError } from "../../../../__data__/slices/loadServerDetail";

export const Main = () => {
  const data = useSelector(serverListData.data);
  const error = useSelector(serverDetail.error);
  const [needRedirect, setNeedRedirect] = useState(false);
  const { serverId } = useParams();
  const dispatch = useDispatch();

  const curentServerId = useSelector(serverDetail.serverId);

  if (needRedirect) {
    return <Page404 />;
  }

  useEffect(() => {
    if (error === "Not Found") {
      dispatch(clearError());
      setNeedRedirect(true);
    }
  }, [error]);

  useEffect(() => {
    // we are on / load the first server when data appears
    if (data && !serverId) {
      dispatch(sumbitDetailRequest(data[0]));
    }
  }, [data]);

  useEffect(() => {
    // we have serverId, load data for this server
    if (serverId) {
      dispatch(sumbitDetailRequest(serverId));
    }
  }, [serverId]);

  return (
    <Dashboard path={curentServerId}>
      <Panels />
    </Dashboard>
  );
};

export default Main;
