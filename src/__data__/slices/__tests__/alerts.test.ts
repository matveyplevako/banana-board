import {
  reducer,
  handleSubmit,
  handleSuccess,
  addItem,
  removeItem,
  failure,
} from "../alerts";
import { submitRequest, submitCreation } from "../../actions/alerts";

import { bananaAxios } from "../../../utils";

import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import { describe, it, expect, jest } from "@jest/globals";

jest.mock("../../../utils/axios.ts", () => ({
  bananaAxios: jest.fn(),
}));

const mockStore = configureMockStore([thunk]);

describe("alerts", () => {
  describe("alerts slices", () => {
    it("alerts запрос данных", async () => {
      const nextState = reducer(undefined, handleSubmit());

      expect(nextState).toEqual({
        loading: true,
        data: null,
        error: null,
      });
    });

    it("alerts получены данные", async () => {
      const nextState = reducer(undefined, handleSuccess(["payload"]));

      expect(nextState).toEqual({
        loading: false,
        data: ["payload"],
        error: null,
      });
    });

    it("alerts добавлен новый item", async () => {
      const success = {
        id: 3,
        name: "No space left",
        server: "Backup Server",
        resource: "MEM",
        value: ">80%",
        active: true,
      };

      const nextState = reducer(
        { loading: false, data: [], error: null },
        addItem(success)
      );

      expect(nextState).toEqual({
        loading: false,
        data: [success],
        error: null,
      });
    });

    it("alerts удаление", () => {
      const initialState = {
        loading: false,
        data: [{ id: "alert1" }, { id: "alert2" }],
        error: null,
      };

      const nextState = reducer(initialState, removeItem("alert1"));

      expect(nextState).toEqual({
        loading: false,
        data: [{ id: "alert2" }],
        error: null,
      });
    });

    it("alerts ошибка при запросе", () => {
      const nextState = reducer(undefined, failure("error message"));

      expect(nextState).toEqual({
        loading: false,
        data: null,
        error: "error message",
      });
    });
  });

  describe("alerts actions", () => {
    it("alerts запрос данных", async () => {
      const success = [{ id: "alert1" }, { id: "alert2" }];
      (bananaAxios as any).mockReturnValue({
        data: success,
      });
      const store = mockStore();
      await store.dispatch(submitRequest());

      const expectedActions = [handleSubmit(), handleSuccess(success)];
      expect(store.getActions()).toEqual(expectedActions);
    });

    it("alerts ошибка", async () => {
      (bananaAxios as any).mockImplementation(() => {
        throw "error";
      });
      const store = mockStore();
      await store.dispatch(submitRequest());

      const expectedActions = [handleSubmit(), failure("error")];
      expect(store.getActions()).toEqual(expectedActions);
    });

    it("alerts создание нового alert", async () => {
      const success = [
        {
          id: 3,
          name: "No space left",
          server: "Backup Server",
          resource: "MEM",
          value: ">80%",
          active: true,
        },
      ];
      (bananaAxios as any).mockReturnValue({
        data: success,
      });
      const store = mockStore();
      await store.dispatch(submitCreation(success));

      const expectedActions = [addItem(success)];
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("alerts ошибка создание нового alert ", async () => {
    const error = { response: { statusText: "Not Found" } };
    const success = [
      {
        id: 3,
        name: "No space left",
        server: "Backup Server",
        resource: "MEM",
        value: ">80%",
        active: true,
      },
    ];
    (bananaAxios as any).mockImplementation(() => {
      throw error;
    });
    const store = mockStore();
    await store.dispatch(submitCreation(success));

    const expectedActions = [failure(error["response"]["statusText"])];
    expect(store.getActions()).toEqual(expectedActions);
  });
});
