import React, { useEffect } from "react";

import style from "./style.css";

import { useSelector, useDispatch } from "react-redux";

import * as alerts from "../../../../../__data__/selectors/alerts";
import { submitRequest } from "../../../../../__data__/actions/alerts";

import { Alert } from "./Alert";
import { Loading } from "../../../../../components/Loading";

const AlertList = () => {
  const list = useSelector(alerts.data);
  const loading = useSelector(alerts.loading);
  const dispatch = useDispatch();

  useEffect(() => {
    if (!list) {
      dispatch(submitRequest());
    }
  }, [list]);

  let items = [
    <div className={style.Loading} key="Loading">
      <Loading />
    </div>,
  ];

  if (!loading) {
    items = list.map((data) => <Alert data={data} key={data["id"]} />);
  }
  return <div className={style.AlertList}>{items}</div>;
};

export default AlertList;
