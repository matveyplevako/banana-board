import React from "react";
import style from "./style.css";
import { useDispatch } from "react-redux";

import { useTranslation } from "react-i18next";
import { Item } from "./Item";
import { clearServerDetail } from "../../../../__data__/actions/loadServerDetail";
import { Dashboard } from "../..";

export const Settings = () => {
  const [t] = useTranslation();
  const dispatch = useDispatch();

  dispatch(clearServerDetail());
  return (
    <Dashboard path={t("dashboard.settings")}>
      <div className={style.Panels}>
        <Item
          id="cpu"
          name={"dashboard.settings.cpu.name"}
          description={t("dashboard.settings.cpu")}
        />
        <Item
          id="ram"
          name={"dashboard.settings.ram.name"}
          description={t("dashboard.settings.ram")}
        />
        <Item
          id="memory"
          name={"dashboard.settings.memory.name"}
          description={t("dashboard.settings.memory")}
        />
        <Item
          id="network"
          name={"dashboard.settings.network.name"}
          description={t("dashboard.settings.network")}
        />
        <Item
          id="characteristic"
          name={"dashboard.settings.characteristic.name"}
          description={t("dashboard.settings.characteristic")}
        />
      </div>
    </Dashboard>
  );
};

export default Settings;
