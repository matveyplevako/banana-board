import React from "react";
import { shallow } from "enzyme";

import { describe, it, expect } from "@jest/globals";

import AuthRequired from "../AuthRequired";

describe("<AuthRequired />", () => {
  it("AuthRequired авторизован", () => {
    Storage.prototype.getItem = () => "token";
    const wrapper = shallow(
      <AuthRequired path="/">
        <div></div>
      </AuthRequired>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("AuthRequired не авторизован", () => {
    Storage.prototype.getItem = () => "";
    const wrapper = shallow(
      <AuthRequired path="/">
        <div></div>
      </AuthRequired>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
