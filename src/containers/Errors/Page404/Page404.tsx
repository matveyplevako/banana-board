import React from "react";

import { ReactLink } from "../../../components/ReactLink";
import { URLs } from "../../../urls";

import { useTranslation } from "react-i18next";

import style from "./style.css";

const Page404 = () => {
  const [t] = useTranslation();
  return (
    <div className={style.Body}>
      <div className={style.Wrapper}>
        <p className={style.Title}>404</p>
        <p className={style.Text}>{t("errors.404.text")}</p>
        <ReactLink to={URLs.home.url}>
          <div className={style.Button} id="Button">
            <p className={style.ButtonText}>{t("errors.404.button")}</p>
          </div>
        </ReactLink>
      </div>
    </div>
  );
};

export default Page404;
