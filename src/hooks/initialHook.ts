import React, { useEffect } from "react";

export const initialHook = (func: React.EffectCallback) => {
  useEffect(func, []);
};
