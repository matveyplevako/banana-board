import React from "react";
import style from "./style.css";
import { Box } from "../../../../../../components/Box";

import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";

function getSpecRepresentation(key, value) {
  const [t] = useTranslation();
  return (
    <div className={style.Item} key={key}>
      <p className={style.Key}>{t(`dashboard.panels.specs.${key}`)}:</p>
      <p className={style.Value}>{value}</p>
    </div>
  );
}

const Specs = (props) => {
  const specsList = props.data;
  const [t] = useTranslation();

  const order = ["cpu", "graphics", "vCPU", "ram", "memory", "IP"];

  const spec = order.map((x) => getSpecRepresentation(x, specsList[x]));

  return (
    <Box title={t("dashboard.panels.specs")} className={style.Specs}>
      <div className={style.SpecsDescription}>{spec}</div>
    </Box>
  );
};

Specs.propTypes = {
  data: PropTypes.object,
};

export default Specs;
