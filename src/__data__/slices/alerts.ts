import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export type Alerts = {
  loading: boolean;
  data: Record<string, string | number>[];
  error: boolean | string;
};

const initialState: Alerts = {
  loading: true,
  data: null,
  error: null,
};

const slice = createSlice({
  name: "alerts",
  initialState,
  reducers: {
    handleSubmit(state) {
      state.loading = true;
    },
    handleSuccess(state, action) {
      state.data = action.payload;
      state.loading = false;
    },
    addItem(state, action) {
      state.data.push(action.payload);
    },
    removeItem(state, action) {
      state.data = state.data.filter((item) => item["id"] !== action.payload);
    },
    failure(state, action: PayloadAction<string | boolean>) {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const {
  handleSubmit,
  handleSuccess,
  addItem,
  removeItem,
  failure,
} = slice.actions;

export const reducer = slice.reducer;
