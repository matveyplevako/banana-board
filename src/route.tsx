import React from "react";
import { Switch, Route } from "react-router-dom";
import { URLs } from "./urls";
import {
  Auth,
  GetStarted,
  Main,
  Alerts,
  Settings,
  Page404,
} from "./containers";

import { AuthRequired } from "./components/AuthRequired";

const Router = () => {
  return (
    <div>
      <Switch>
        <Route exact path={URLs.home.url}>
          <GetStarted />
        </Route>
        <Route path={URLs.auth.url}>
          <Auth />
        </Route>
        <AuthRequired path={`${URLs.dashboard.url}:serverId`}>
          <Main />
        </AuthRequired>
        <AuthRequired path={URLs.dashboard.url}>
          <Main />
        </AuthRequired>
        <AuthRequired path={URLs.settings.url}>
          <Settings />
        </AuthRequired>
        <AuthRequired path={URLs.alerts.url}>
          <Alerts />
        </AuthRequired>
        <Route path="*">
          <Page404 />
        </Route>
      </Switch>
    </div>
  );
};

export { Router };
