import { bananaAxios } from "../../utils";

import { handleSubmit, handleSuccess, failure } from "../slices/loadServerList";

export const submitRequest = () => async (dispatch) => {
  dispatch(handleSubmit());
  try {
    const response = await bananaAxios("/serverList", {
      method: "GET",
    });
    dispatch(handleSuccess(response.data.serverList));
  } catch (error) {
    dispatch(failure(error));
  }
};
