import React from "react";
import style from "./style.css";

import { useTranslation } from "react-i18next";

import { Manage } from "./Manage";
import { DoughnutChart } from "./DoughnutChart";
import { Network } from "./Network";
import { Specs } from "./Specs";
import { Loading } from "../../../../../components/Loading";

import { useSelector } from "react-redux";

import * as serverDetail from "../../../../../__data__/selectors/loadServerDetail";

const Panels = () => {
  const [t] = useTranslation();
  const data = useSelector(serverDetail.data);
  const loading = useSelector(serverDetail.loading);

  let body;
  if (data) {
    const network_component =
      window.localStorage.getItem("dashboard.settings.network.name") !==
      "off" ? (
          <Network data={data["Network"]} />
        ) : (
          <React.Fragment />
        );
    const cpu_component =
      window.localStorage.getItem("dashboard.settings.cpu.name") !== "off" ? (
        <DoughnutChart name={t("dashboard.panels.cpu")} load={data["CPU"]} />
      ) : (
        <React.Fragment />
      );
    const ram_component =
      window.localStorage.getItem("dashboard.settings.ram.name") !== "off" ? (
        <DoughnutChart name={t("dashboard.panels.ram")} load={data["RAM"]} />
      ) : (
        <React.Fragment />
      );
    const mem_component =
      window.localStorage.getItem("dashboard.settings.memory.name") !==
      "off" ? (
          <DoughnutChart name={t("dashboard.panels.memory")} load={data["MEM"]} />
        ) : (
          <React.Fragment />
        );
    const specs_component =
      window.localStorage.getItem("dashboard.settings.characteristic.name") !==
      "off" ? (
          <Specs data={data["Specs"]} />
        ) : (
          <React.Fragment />
        );
    body = (
      <div className={style.Panels}>
        {network_component}
        <Manage serverId={data["serverId"]} data={data["serverState"]} />
        {cpu_component}
        {ram_component}
        {mem_component}
        {specs_component}
      </div>
    );
  }

  return (
    <React.Fragment>
      <div className={`${style.Loading} ${loading ? "" : style.HideLoading}`}>
        <Loading />
      </div>
      {body}
    </React.Fragment>
  );
};

export default Panels;
