import React from "react";
import style from "./style.css";

import { Link } from "react-router-dom";

import PropTypes from "prop-types";

const ReactLink = (props) => {
  return (
    <Link className={`${style.ahref} ${props.className}`} to={props.to}>
      {props.children}
    </Link>
  );
};

ReactLink.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string,
  children: PropTypes.node,
};

ReactLink.defaultProps = {
  className: "",
};

export default ReactLink;
