import React, { useEffect } from "react";
import style from "./style.css";

import PropTypes from "prop-types";
import { useSelector } from "react-redux";

import { LeftPanel } from "./LeftPanel";
import { Navbar } from "./Navbar";
import { useDispatch } from "react-redux";

import { submitRequest as submitServerListRequest } from "../../__data__/actions/loadServerList";
import { data as dataSelector } from "../../__data__/selectors/loadServerList";

export const Dashboard = (props) => {
  const dispatch = useDispatch();
  const data = useSelector(dataSelector);

  useEffect(() => {
    if (!data) {
      dispatch(submitServerListRequest());
    }
  });

  return (
    <div className={style.Dashboard}>
      <LeftPanel />
      <div className={style.MainBoard}>
        <Navbar path={props.path} />
        {props.children}
      </div>
    </div>
  );
};

Dashboard.propTypes = {
  path: PropTypes.string,
  children: PropTypes.node,
};

export default Dashboard;
