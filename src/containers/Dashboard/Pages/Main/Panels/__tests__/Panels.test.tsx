import React from "react";
import { mount } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";
import { BrowserRouter } from "react-router-dom";
import { useSelector } from "react-redux";

import Panels from "../Panels";
import * as serverDetail from "../../../../../../__data__/selectors/loadServerDetail";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: () => jest.fn(),
}));

jest.mock("react-chartjs-2", () => ({
  Doughnut: () => "Doughnut",
  Bar: () => "Bar",
}));


describe("<Panels />", () => {
  it("Panels загрузка", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case serverDetail.data:
        return undefined;
      case serverDetail.loading:
        return true;
      }
    });
    const wrapper = mount(
      <BrowserRouter>
        <Panels />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("Panels с данными", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case serverDetail.data:
        return {
          serverId: "Server1",
          CPU: 35,
          MEM: 60,
          RAM: 90,
          Network: [],
          serverState: true,
          Specs: {
            cpu: "i7",
            graphics: "RTX 3080",
            vCPU: "8",
            ram: "16GB",
            memory: "1TB",
            IP: "10.12.31.23",
          },
        };
      case serverDetail.loading:
        return false;
      }
    });
    jest.spyOn(Date.prototype, "getHours").mockReturnValue(0);
    jest.spyOn(Date.prototype, "getMinutes").mockReturnValue(10);

    const wrapper = mount(
      <BrowserRouter>
        <Panels />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("Panels выключены все элементы интерфейса", () => {
    Storage.prototype.getItem = jest.fn(() => "off");
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case serverDetail.data:
        return [];
      case serverDetail.loading:
        return false;
      }
    });
    const wrapper = mount(
      <BrowserRouter>
        <Panels />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
