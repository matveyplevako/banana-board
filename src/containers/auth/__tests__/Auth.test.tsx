import React from "react";
import { mount } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";
import { useSelector } from "react-redux";
import { BrowserRouter } from "react-router-dom";

import * as loading from "../../../__data__/selectors/login";

import { Auth } from "../Auth";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: () => jest.fn(),
}));

describe("<Auth />", () => {
  it("Auth empty submit", () => {
    const wrapper = mount(
      <BrowserRouter>
        <Auth />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();

    wrapper.find("#signIn").simulate("submit");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("Auth simulate login and password and then submit", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case loading.login:
        return "login";
      case loading.password:
        return "password";
      }
    });
    const wrapper = mount(
      <BrowserRouter>
        <Auth />
      </BrowserRouter>
    );

    wrapper.find("#signIn").simulate("submit");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();

    wrapper
      .find("#login")
      .last()
      .simulate("change", { target: { value: "Hello" } });
    wrapper
      .find("#password")
      .last()
      .simulate("change", { target: { value: "World" } });
    wrapper.update();
    expect(wrapper).toMatchSnapshot();

    wrapper.find("#signIn").simulate("submit");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });

  it("Auth login completed", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case loading.data:
        return "token";
      }
    });
    const wrapper = mount(
      <BrowserRouter>
        <Auth />
      </BrowserRouter>
    );
    expect(wrapper.find("Redirect").length).toEqual(1);
  });
});
