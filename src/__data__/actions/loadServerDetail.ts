import { bananaAxios } from "../../utils";

import {
  handleSubmit,
  handleSuccess,
  changeServerId,
  clear,
  failure,
} from "../slices/loadServerDetail";

export const submitRequest = (serverId: string) => async (dispatch) => {
  dispatch(changeServerId(serverId));
  dispatch(handleSubmit());
  try {
    const response = await bananaAxios(`/server/${serverId}`, {
      method: "GET",
    });
    dispatch(handleSuccess(response.data));
  } catch (error) {
    dispatch(failure(error.response.statusText));
  }
};

export { clear as clearServerDetail };
