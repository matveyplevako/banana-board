import React from "react";
import { mount } from "enzyme";

import { describe, it, expect } from "@jest/globals";
import { BrowserRouter } from "react-router-dom";

import { GetStarted } from "../GetStarted";

describe("<GetStarted />", () => {
  it("GetStarted рендерится без ошибок", () => {
    Storage.prototype.getItem = () => "";
    const wrapper = mount(
      <BrowserRouter>
        <GetStarted />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("GetStarted dashboard redirect", () => {
    Storage.prototype.getItem = () => "token";
    const wrapper = mount(
      <BrowserRouter>
        <GetStarted />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
