import style from "./style.css";
import React from "react";
import { Doughnut } from "react-chartjs-2";
import { Box } from "../../../../../../components/Box";

import PropTypes from "prop-types";

function getData(percent) {
  const colors = {
    red: "rgba(235, 87, 87, 1)",
    green: "rgba(39, 174, 96, 1)",
    yellow: "rgba(255, 211, 55, 1)",
  };

  const selectedColor =
    percent < 50 ? "green" : percent < 75 ? "yellow" : "red";

  return {
    datasets: [
      {
        data: [percent, 100 - percent],
        backgroundColor: [colors[selectedColor], "rgba(255, 255, 255, 1)"],
        borderColor: [colors[selectedColor], colors[selectedColor]],
        borderWidth: 1,
      },
    ],
  };
}

export const options = {
  cutoutPercentage: 80,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  tooltips: {
    enabled: false,
  },
};
const DoughnutChart = (props) => {
  return (
    <Box title={props.name}>
      <div className={style.zstack}>
        <Doughnut data={getData(props.load)} options={options} />
        <p>{props.load}%</p>
      </div>
    </Box>
  );
};

DoughnutChart.propTypes = {
  name: PropTypes.string,
  load: PropTypes.number,
};

export default DoughnutChart;
