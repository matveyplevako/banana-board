import React, { useEffect } from "react";

import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";

import { clearServerDetail } from "../../../../__data__/actions/loadServerDetail";
import { Dashboard } from "../..";
import { AlertList } from "./AlertList";
import { AlertsCreate } from "./AlertsCreate";

import style from "./style.css";

const Alerts = () => {
  const [t] = useTranslation();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(clearServerDetail());
  }, []);

  return (
    <Dashboard path={t("dashboard.alerts")}>
      <div className={style.Main}>
        <div className={`${style.List} ${style.Header}`}>
          <p />
          <p className={style.Name}>{t("dashboard.alerts.name")}</p>
          <p>{t("dashboard.alerts.server")}</p>
          <p>{t("dashboard.alerts.resource")}</p>
          <p>{t("dashboard.alerts.value")}</p>
          <p>{t("dashboard.alerts.manage")}</p>
        </div>
        <AlertsCreate />
        <AlertList />
      </div>
    </Dashboard>
  );
};

export default Alerts;
