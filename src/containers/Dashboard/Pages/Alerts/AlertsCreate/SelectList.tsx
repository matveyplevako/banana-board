import React, { useState } from "react";

import style from "./select.css";
import { Arrow } from "../../../../../assets";

import PropTypes from "prop-types";

export const SelectList = (props) => {
  const [show, setShow] = useState(false);
  const [selectedItem, setSelectedItem] = useState();

  const updateServer = (serverId) => {
    setSelectedItem(serverId);
    props.onSet(serverId);
  };

  let options = [];
  if (props.list) {
    options = props.list.map((x) => {
      return (
        <div
          key={x}
          onClick={() => {
            props.unsetEmpty();
            updateServer(x);
          }}
        >
          {x}
        </div>
      );
    });
  }
  return (
    <div
      className={`${style.Dropdown} ${show ? style.Open : ""} `}
      onClick={() => setShow(!show)}
    >
      <div
        className={`${style.Dropbtn} ${!selectedItem ? style.Gray : ""} ${
          props.className
        }`}
      >
        <img
          id="showMenu"
          src={Arrow}
          className={`${style.Arrow} ${show ? style.Open : ""}`}
        />
        {selectedItem ? selectedItem : props.placeholder}
      </div>
      <div className={style.DropdownContent}>{options}</div>
    </div>
  );
};

SelectList.propTypes = {
  id: PropTypes.string,
  unsetEmpty: PropTypes.func,
  onSet: PropTypes.func,
  list: PropTypes.array,
  className: PropTypes.string,
  placeholder: PropTypes.string,
};
