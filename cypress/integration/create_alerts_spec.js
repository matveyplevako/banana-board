describe("Alerts test", () => {
    it("Visits the dashboard and test alerts", () => {
        cy.visit("/auth");

        cy.url().should("include", "/auth");

        cy.get("#login")
            .type("saymyname@yandex.ru")
            .should("have.value", "saymyname@yandex.ru");

        cy.get("#password").type("qwerty123").should("have.value", "qwerty123");

        cy.get("#signIn").click();


        cy.get('[href="/banana-board/alerts"]').click();

        cy.get('#name')
            .type("NewAlert1")
            .should("have.value", "NewAlert1");

        cy.get('#server')
            .type("Server 2")
            .should("have.value", "Server 2");

        cy.get('#resource')
            .type("GPU")
            .should("have.value", "GPU");

        cy.get('#value')
            .type(">10%")
            .should("have.value", ">10%");

        cy.get('._-_-_-_-_-_-_-src-containers-Dashboard-Pages-Alerts-AlertsCreate---style__RoundedBorders--2zf').click();

        cy.get(':nth-child(4) > :nth-child(7) > img').click();
        cy.get(':nth-child(1) > :nth-child(7) > img').click();
        cy.get(':nth-child(1) > :nth-child(7) > img').click();
        cy.get(':nth-child(7) > img').click();

        cy.contains('turn on').should('not.exist');
    });
});
