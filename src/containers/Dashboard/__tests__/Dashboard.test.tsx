import React from "react";
import { shallow } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";
import { useSelector } from "react-redux";

import Dashboard from "../Dashboard";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: () => jest.fn(),
}));

jest.spyOn(React, "useEffect").mockImplementation((func) => func());

describe("<Dashboard />", () => {
  it("ServerList пустой", () => {
    (useSelector as any).mockImplementation(() => false);
    const wrapper = shallow(<Dashboard />);
    expect(wrapper).toMatchSnapshot();
  });

  it("ServerList non empty", () => {
    (useSelector as any).mockImplementation(() => true);
    const wrapper = shallow(<Dashboard />);
    expect(wrapper).toMatchSnapshot();
  });
});
