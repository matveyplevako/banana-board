import React from "react";
import { mount } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";
import { BrowserRouter } from "react-router-dom";
import { useSelector } from "react-redux";

import LeftPanel from "../LeftPanel";
import * as serverListData from "../../../../__data__/selectors/loadServerList";
import * as serverDetail from "../../../../__data__/selectors/loadServerDetail";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: () => jest.fn(),
}));

describe("<LeftPanel />", () => {
  it("LeftPanel загрузка", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case serverListData.data:
        return [];
      case serverListData.loading:
        return true;
      case serverDetail.serverId:
        return "";
      }
    });
    const wrapper = mount(
      <BrowserRouter>
        <LeftPanel />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });

  it("LeftPanel с данными", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case serverListData.data:
        return ["Server1", "Server2"];
      case serverListData.loading:
        return false;
      case serverDetail.serverId:
        return "Server1";
      }
    });
    const wrapper = mount(
      <BrowserRouter>
        <LeftPanel />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
