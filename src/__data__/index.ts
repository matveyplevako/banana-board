import { configureStore } from "@reduxjs/toolkit";

import {
  reducer as serverListReduser,
  ServerListState,
} from "./slices/loadServerList";
import {
  reducer as serverDetailReducer,
  ServerDetailState,
} from "./slices/loadServerDetail";
import { reducer as alertReducer, Alerts } from "./slices/alerts";
import { reducer as loginReducer, LoginState } from "./slices/login";
import { combineReducers } from "redux";

export type AppStore = {
  login: LoginState;
  loadServerList: ServerListState;
  loadServerDetail: ServerDetailState;
  alerts: Alerts;
};

const appReducer = combineReducers<AppStore>({
  login: loginReducer,
  loadServerList: serverListReduser,
  loadServerDetail: serverDetailReducer,
  alerts: alertReducer,
});

export default appReducer;

export const store = configureStore({
  reducer: appReducer,
});
