import React from "react";
import PropTypes from "prop-types";
import style from "./style.css";

const Box = (props) => {
  return (
    <div className={`${style.WhiteBox} ${props.className}`}>
      <p className={style.BoxText}>{props.title}</p>
      {props.children}
    </div>
  );
};

Box.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  children: PropTypes.node,
};

export default Box;
