describe("First Banana Test of the test", () => {
    it("Visits the banana board and get started", () => {
        cy.visit("/");

        cy.get("#welcomeButton").click();

        cy.url().should("include", "/auth");

        cy.get("#login")
            .type("saymyname@yandex.ru")
            .should("have.value", "saymyname@yandex.ru");

        cy.get("#password").type("qwerty123").should("have.value", "qwerty123");

        cy.get("#signIn").click();
    });
});
