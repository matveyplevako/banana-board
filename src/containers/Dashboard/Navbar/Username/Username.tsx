import React, { useState } from "react";
import style from "./style.css";
import { ReactLink } from "../../../../components/ReactLink";

import { useTranslation } from "react-i18next";

import { Arrow } from "../../../../assets";
import { URLs } from "../../../../urls";
import { useDispatch } from "react-redux";
import { logout } from "../../../../__data__/slices/login";

const Username = () => {
  const dispatch = useDispatch();
  const [t] = useTranslation();

  const [showMenu, setShowMenu] = useState(false);
  return (
    <div className={style.Menu}>
      <img
        id="showMenu"
        src={Arrow}
        className={`${style.Arrow} ${showMenu ? style.Open : ""}`}
        onClick={() => setShowMenu(!showMenu)}
      />
      <div style={{ marginLeft: "20px" }}>
        <p>username</p>
      </div>
      <div
        className={`${style.OptionsContainer} ${showMenu ? "" : style.Hide}`}
      >
        <p>{t("dashboard.navbar.user.role")}</p>
        <ReactLink to={URLs.home.url}>
          <div
            id="exit"
            className={style.RoundedBorders}
            onClick={() => {
              window.localStorage.removeItem("banana:token");
              dispatch(logout());
            }}
          >
            {t("dashboard.navbar.user.signOut")}
          </div>
        </ReactLink>
      </div>
    </div>
  );
};

export default Username;
