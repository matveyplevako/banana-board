import React from "react";
import { mount } from "enzyme";

import { describe, it, expect } from "@jest/globals";

import { Input } from "../Input";

describe("<Input />", () => {
  it("Input рендерится без ошибок и меняет значение", () => {
    let value = "test";

    const handleChange = (event) => {
      value = event.target.value;
    };

    const wrapper = mount(
      <Input
        onChange={handleChange}
        type="text"
        placeholder="placeholder"
        value={value}
        id="login"
        disabled={false}
      />
    );

    const element = wrapper.find("input");
    expect(element).toMatchSnapshot();

    element.simulate("change", { target: { value: "Hello" } });
    wrapper.setProps({ value });
    wrapper.update();
    expect(wrapper.find("input")).toMatchSnapshot();
  });
});
