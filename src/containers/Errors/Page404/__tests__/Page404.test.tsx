import React from "react";
import { mount } from "enzyme";

import { describe, it, expect } from "@jest/globals";
import { BrowserRouter } from "react-router-dom";

import Page404 from "../Page404";

describe("<Page404 />", () => {
  it("Page404 рендерится без ошибок", () => {
    const wrapper = mount(
      <BrowserRouter>
        <Page404 />
      </BrowserRouter>
    );
    expect(wrapper).toMatchSnapshot();
  });
});
