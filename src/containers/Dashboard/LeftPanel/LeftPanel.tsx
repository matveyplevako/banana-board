import React from "react";
import style from "./style.css";
import { ReactLink } from "../../../components/ReactLink";

import { useTranslation } from "react-i18next";

import { ServerList } from "./ServerList";
import { URLs } from "../../../urls";

import { Notification, Settings } from "../../../assets";

const LeftPanel = () => {
  const [t] = useTranslation();

  return (
    <div className={style.LeftPanel}>
      <ReactLink className={style.Title} to={URLs.dashboard.url}>
        <div className={style.Title}>
          <h1>BananaBoard</h1>
        </div>
      </ReactLink>
      <ServerList />
      <div className={style.SettingsList}>
        <div></div>
        <ReactLink to={URLs.settings.url}>
          <div className={style.Settings}>
            <img src={Settings} />
            <p className={style.SettingElementText}>
              {t("dashboard.leftPanel.settings")}
            </p>
          </div>
        </ReactLink>
        <ReactLink to={URLs.alerts.url}>
          <div className={style.Settings}>
            <img src={Notification} />
            <p className={style.SettingElementText}>
              {t("dashboard.leftPanel.alerts")}
            </p>
          </div>
        </ReactLink>
        <div></div>
      </div>
    </div>
  );
};

export default LeftPanel;
