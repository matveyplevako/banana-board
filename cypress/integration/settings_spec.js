describe("Settings page test", () => {
    it("Visits the settings page and get started", () => {
        cy.visit("/auth");

        cy.url().should("include", "/auth");

        cy.get("#login")
            .type("saymyname@yandex.ru")
            .should("have.value", "saymyname@yandex.ru");

        cy.get("#password").type("qwerty123").should("have.value", "qwerty123");

        cy.get("#signIn").click();

        cy.get('[href="/banana-board/settings"]').click();


        // turn off characteristic
        cy.get('#characteristic').click();

        cy.get('[href="/banana-board/dashboard/Server1"]').click();

        cy.contains('Manage');
        cy.contains('Dashboard-Pages-Main-Panels-Specs').should('not.exist');

        cy.contains("settings").click();

        cy.get('#characteristic').click();


        // turn off CPU
        cy.get('#cpu').click();

        cy.get('[href="/banana-board/dashboard/Server1"]').click();

        cy.contains('Manage');
        cy.contains('_-_-_-_-_-_-_-src-components-Box---style__BoxText--1j-').should("not.exist");

        cy.contains("settings").click();

        cy.get('#cpu').click();


        // turn off memory
        cy.get('#memory').click();
        cy.get('#characteristic').click();

        cy.get('[href="/banana-board/dashboard/Server1"]').click();

        cy.contains('Manage');
        cy.contains('Memory').should('not.exist');

        cy.contains("settings").click();

        cy.get('#memory').click();
        cy.get('#characteristic').click();


        // turn off RAM
        cy.get('#ram').click();

        cy.get('[href="/banana-board/dashboard/Server1"]').click();

        cy.contains('Manage');
        cy.contains('_-_-_-_-_-_-_-src-components-Box---style__BoxText--1j-').should('not.exist');

        cy.contains("settings").click();

        cy.get('#ram').click();


        // turn off network
        cy.get('#network').click();

        cy.get('[href="/banana-board/dashboard/Server1"]').click();

        cy.contains('Manage');
        cy.contains('Dashboard-Pages-Main-Panels-Network').should('not.exist');

        cy.contains("settings").click();

        cy.get('#network').click();
    });
});
