import React from "react";
import { mount } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";

import Item from "../Item";

jest.mock("react-redux", () => ({
  useDispatch: () => jest.fn(),
}));

jest.spyOn(React, "useEffect").mockImplementation((func) => func());

describe("<Item />", () => {
  it("Item", () => {
    Storage.prototype.getItem = jest.fn(() => null);
    Storage.prototype.setItem = jest.fn(() => undefined);

    const wrapper = mount(<Item id="switch" />);
    expect(wrapper).toMatchSnapshot();

    const elem = wrapper.find("#switch").last();
    elem.simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();

    elem.simulate("click");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();
  });
});
