import { createSelector } from "reselect";

import { AppStore } from "..";
import { ServerDetailState } from "../slices/loadServerDetail";

const rootSelector = createSelector<AppStore, AppStore, ServerDetailState>(
  (state) => state,
  (state) => state.loadServerDetail
);

export const loading = createSelector(rootSelector, (state) => state.loading);
export const data = createSelector(rootSelector, (state) => state.data);
export const serverId = createSelector(rootSelector, (state) => state.serverId);
export const error = createSelector(rootSelector, (state) => state.error);
