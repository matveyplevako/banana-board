import React, { useState } from "react";
import style from "./style.css";

import i18next from "i18next";
import { useTranslation } from "react-i18next";

import PropTypes from "prop-types";

import { RU, US, Notification } from "../../../assets";
import { Notifications } from "./Notifications";
import { Username } from "./Username";

const Navbar = (props) => {
  const [t] = useTranslation();
  const [language, setLanguage] = useState(i18next.language === "ru");
  const [showNotificationList, setShowNotificationList] = useState(false);

  const path = props.path;

  const changeLanguage = () => {
    if (i18next.language === "ru") {
      i18next.changeLanguage("en");
    } else {
      i18next.changeLanguage("ru");
    }
  };

  return (
    <div className={style.Navbar}>
      <div className={style.Route}>
        <p className={style.RouteText}>Dashboard/{path}</p>
      </div>
      <div className={style.Controls}>
        <div
          id="languageSwitch"
          className={style.Circle}
          onClick={() => {
            changeLanguage();
            setLanguage(!language);
          }}
          title={t("dashboard.navbar.lang")}
        >
          <img id={language ? "RU_svg" : "ENG_svg"} src={language ? RU : US} />
        </div>
        <div
          id="notifications"
          className={`
            ${style.Circle} ${
      showNotificationList ? style.NotificationsOpened : ""
    }`}
          onClick={() => setShowNotificationList(!showNotificationList)}
        >
          <img src={Notification} />
        </div>
        <Notifications showNotificationList={showNotificationList} />
        <Username />
      </div>
    </div>
  );
};

Navbar.propTypes = {
  path: PropTypes.string,
};

export default Navbar;
