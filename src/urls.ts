import { getNavigations } from "@ijl/cli";

const navigations = getNavigations("banana-board");

export const baseUrl = navigations["banana-board"];

export const URLs = {
  home: {
    url: navigations["banana-board"],
  },
  auth: {
    url: navigations["banana-board.auth"],
  },
  dashboard: {
    url: navigations["banana-board.dashboard"],
  },
  settings: {
    url: navigations["banana-board.settings"],
  },
  alerts: {
    url: navigations["banana-board.alerts"],
  },
};
