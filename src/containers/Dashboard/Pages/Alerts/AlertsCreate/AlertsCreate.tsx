import React, { useState, useEffect } from "react";

import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";

import { Input } from "../../../../../components/Input";
import { SelectList } from "./SelectList";
import style from "./style.css";

import { submitCreation } from "../../../../../__data__/actions/alerts";
import { data } from "../../../../../__data__/selectors/loadServerList";

const AlertsCreate = () => {
  const [t] = useTranslation();
  const dispatch = useDispatch();

  const specsList = ["CPU", "RAM", "MEM"];
  const conditionsList = [">", "<"];

  const serverList = useSelector(data);

  const emptyInitialState = {
    name: true,
    server: true,
    resource: true,
    value: true,
    condition: true,
  };

  const initialState = {
    name: "",
    server: "",
    resource: "",
    condition: "",
    value: "",
  };

  const [newAlert, setNewAlert] = useState(initialState);
  const [empty, setEmpty] = useState(emptyInitialState);

  useEffect(() => {
    setEmpty({
      name: false,
      server: false,
      resource: false,
      value: false,
      condition: false,
    });
  }, []);

  function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();

    let hasEmpty = false;
    const newState = emptyInitialState;
    for (const [key, value] of Object.entries(newAlert)) {
      newState[key] = value == "";
      if (newState[key]) {
        hasEmpty = true;
      }
    }
    if (hasEmpty) {
      setEmpty(newState);
      return;
    }

    const data = {
      ...newAlert,
      value: `${newAlert["condition"]}${newAlert["value"]}%`,
    };
    dispatch(submitCreation(data));
  }
  return (
    <form className={`${style.List} ${style.AddNew}`} onSubmit={handleSubmit}>
      <Input
        onChange={(event) => {
          if (empty["name"]) {
            setEmpty({ ...empty, name: false });
          }
          setNewAlert({ ...newAlert, name: event.target.value });
        }}
        type="text"
        className={`${style.InputBorders} ${style.Name} ${
          empty["name"] ? style.Red : ""
        }`}
        placeholder={t("dashboard.alerts.name")}
        id="name"
      />
      <SelectList
        id="server"
        list={serverList}
        unsetEmpty={() => setEmpty({ ...empty, server: false })}
        className={empty["server"] ? style.Red : ""}
        placeholder={t("dashboard.alerts.server")}
        onSet={(value) => setNewAlert({ ...newAlert, server: value })}
      />
      <SelectList
        id="resource"
        list={specsList}
        unsetEmpty={() => setEmpty({ ...empty, resource: false })}
        className={empty["resource"] ? style.Red : ""}
        placeholder={t("dashboard.alerts.resource")}
        onSet={(value) => setNewAlert({ ...newAlert, resource: value })}
      />
      <div className={style.Value}>
        <SelectList
          id="conditions"
          unsetEmpty={() => setEmpty({ ...empty, condition: false })}
          className={`${style.HalfConditions} ${
            empty["condition"] ? style.Red : ""
          }`}
          list={conditionsList}
          onSet={(value) => setNewAlert({ ...newAlert, condition: value })}
        />
        <Input
          onChange={(event) => {
            if (empty["value"]) {
              setEmpty({ ...empty, value: false });
            }
            setNewAlert({
              ...newAlert,
              value: event.target.value,
            });
          }}
          type="number"
          min="0"
          max="100"
          className={`${style.InputBorders} ${style.Half} ${
            empty["value"] ? style.Red : ""
          }`}
          placeholder={t("dashboard.alerts.value")}
          id="value"
          disabled={false}
        />
      </div>
      <button id="create" className={style.RoundedBorders} type="submit">
        {t("dashboard.alerts.createButton")}
      </button>
    </form>
  );
};

export default AlertsCreate;
