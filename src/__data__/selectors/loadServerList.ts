import { createSelector } from "reselect";

import { AppStore } from "..";
import { ServerListState } from "../slices/loadServerList";

const rootSelector = createSelector<AppStore, AppStore, ServerListState>(
  (state) => state,
  (state) => state.loadServerList
);

export const loading = createSelector(rootSelector, (state) => state.loading);
export const data = createSelector(rootSelector, (state) => state.data);
export const error = createSelector(rootSelector, (state) => state.error);
