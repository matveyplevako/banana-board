import { bananaAxios } from "../../utils";

import {
  failure,
  handleSubmit,
  handleSuccess,
  handleSetFormLogin,
  handleSetFormPassword,
  handleSetValidationError,
} from "../slices/login";
import md5 from "md5";
import { Dispatch } from "redux";

export const checkLogin = () => async (dispatch) => {
  const token = window.localStorage.getItem("banana:token");
  if (token) {
    dispatch(handleSuccess(token));
  }
};

export const submitLogin = ({ login, password }) => async (
  dispatch: Dispatch
) => {
  dispatch(handleSubmit());
  try {
    const response = await bananaAxios("/login", {
      method: "POST",
      data: { login, password: md5(password) },
    });
    window.localStorage.setItem("banana:token", response.data.token);
    dispatch(handleSuccess(response.data.token));
  } catch (error) {
    dispatch(failure(error.response));
  }
};

export { handleSetFormLogin, handleSetFormPassword, handleSetValidationError };
