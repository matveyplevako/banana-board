import React, { useState } from "react";

import style from "./style.css";

import { useDispatch } from "react-redux";

import { useTranslation } from "react-i18next";
import PropTypes from "prop-types";

import { DeleteAlert } from "../../../../../../assets";
import { removeItem } from "../../../../../../__data__/slices/alerts";

const Alert = (props) => {
  const [t] = useTranslation();
  const data = props.data;

  const dispatch = useDispatch();
  const [active, setActive] = useState(data["active"]);

  return (
    <div className={style.Alert}>
      <div
        className={`${style.Status} ${active ? style.Active : style.InActive}`}
      />
      <p>{data["name"]}</p>
      <p>{data["server"]}</p>
      <p>{data["resource"]}</p>
      <p>{data["value"]}</p>
      <p
        className={`${style.RoundedBorders} ${
          active ? style.RedText : style.GreenText
        }`}
        onClick={() => {
          setActive(!active);
        }}
      >
        {active ? t("dashboard.alerts.turnOff") : t("dashboard.alerts.turnOn")}
      </p>
      <p
        className={`${style.RoundedBorders}`}
        onClick={() => {
          dispatch(removeItem(data["id"]));
        }}
      >
        <img src={DeleteAlert} />
      </p>
    </div>
  );
};

Alert.propTypes = {
  data: PropTypes.object,
};

export default Alert;
