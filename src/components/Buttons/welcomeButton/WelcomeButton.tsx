import React from "react";

import { useTranslation } from "react-i18next";

import style from "./style.css";
import { URLs } from "../../../urls";
import { ReactLink } from "../../ReactLink";

const WelcomeButton = () => {
  const [t] = useTranslation();
  return (
    <ReactLink
      to={
        window.localStorage.getItem("banana:token")
          ? URLs.dashboard.url
          : URLs.auth.url
      }
    >
      <div className={style.welcomeButton} id="welcomeButton">
        <p className={style.welcomeButtonText}>
          {t("dashboard.welcome.button")}
        </p>
      </div>
    </ReactLink>
  );
};

export { WelcomeButton };
