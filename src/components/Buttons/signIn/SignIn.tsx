import React from "react";

import { useTranslation } from "react-i18next";

import { Loading } from "../../Loading";

import PropTypes from "prop-types";

import style from "./style.css";

const SignIn = (props) => {
  const [t] = useTranslation();

  if (props.showLoading) {
    return <Loading />;
  }
  return (
    <button
      type="submit"
      className={`${style.authButtonLeft} ${style.authButton}`}
      id="signIn"
    >
      <p className={`${style.authButtonText} ${style.authButtonLeftText}`}>
        {t("dashboard.auth.signIn")}
      </p>
    </button>
  );
};

SignIn.propTypes = {
  showLoading: PropTypes.bool,
};

export { SignIn };
