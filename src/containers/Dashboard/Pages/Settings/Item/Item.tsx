import React, { useState } from "react";
import { Box } from "../../../../../components/Box";
import style from "./style.css";

import PropTypes from "prop-types";

import { useTranslation } from "react-i18next";

export const Item = (props) => {
  const [t] = useTranslation();
  if (window.localStorage.getItem(props.name) === null) {
    window.localStorage.setItem(props.name, "on");
  }
  const on = window.localStorage.getItem(props.name) === "on";
  const [isOn, setisOn] = useState(on);
  return (
    <Box title={t(props.name)}>
      <div className={style.Item}>
        <div className={style.Description}>{props.description}</div>
        <p
          id={props.id}
          className={`${style.RoundedBorders} ${
            isOn ? style.RedText : style.GreenText
          }`}
          onClick={() => {
            setisOn(!isOn);
            window.localStorage.setItem(props.name, !isOn ? "on" : "off");
          }}
        >
          {isOn
            ? t("dashboard.settings.turnOff")
            : t("dashboard.settings.turnOn")}
        </p>
      </div>
    </Box>
  );
};

Item.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  description: PropTypes.string,
};

export default Item;
