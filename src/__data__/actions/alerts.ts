import { Dispatch } from "redux";
import { bananaAxios } from "../../utils";

import {
  handleSubmit,
  handleSuccess,
  addItem,
  failure,
} from "../slices/alerts";

export const submitRequest = () => async (dispatch: Dispatch) => {
  dispatch(handleSubmit());
  try {
    const response = await bananaAxios("/alerts", {
      method: "GET",
    });
    dispatch(handleSuccess(response.data));
  } catch (error) {
    dispatch(failure(error));
  }
};

export const submitCreation = (data) => async (dispatch: Dispatch) => {
  try {
    const response = await bananaAxios("/alerts", {
      method: "POST",
      data: { ...data },
    });
    dispatch(addItem(response.data));
  } catch (error) {
    dispatch(failure(error.response.statusText));
  }
};
