import React from "react";
import style from "./style.css";

const Loading = () => {
  return (
    <div className={style["lds-dual-ring"]}>
      <div className={style["lds-dual-ring-after"]}></div>
    </div>
  );
};

export default Loading;
