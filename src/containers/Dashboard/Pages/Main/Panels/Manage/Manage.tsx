import React, { useState, useEffect } from "react";
import style from "./style.css";

import { useTranslation } from "react-i18next";
import { bananaAxios } from "../../../../../../utils";

import PropTypes from "prop-types";

import { Box } from "../../../../../../components/Box";

const Manage = (props) => {
  const [t] = useTranslation();
  const [serverState, setServerState] = useState(props.data);

  useEffect(() => {
    setServerState(props.data);
  }, [props]);

  const [switching, setSwitching] = useState(false);

  const switchServerState = async () => {
    if (switching) return;
    setSwitching(true);
    const action = { action: serverState ? "turnOff" : "turnOn" };
    try {
      await bananaAxios(`/server/${props.serverId}/manage`, {
        method: "POST",
        data: action,
      });
      setServerState(!serverState);
      setSwitching(false);
      // setServers(response.data);
    } catch (error) {
      console.log("");
    }
  };

  const reloadServer = async () => {
    if (switching) return;
    setSwitching(true);
    try {
      await bananaAxios(`/server/${props.serverId}/manage`, {
        method: "POST",
        data: { action: "reload" },
      });
      setSwitching(false);
      // setServers(response.data);
    } catch (error) {
      console.log("");
    }
  };

  const button = (
    <p
      id="switch"
      className={`${style.RoundedBorders}
        ${
    switching
      ? style.GrayText
      : serverState
        ? style.RedText
        : style.GreenText
    }
          `}
      onClick={() => switchServerState()}
    >
      {switching
        ? t("dashboard.manage.Wait")
        : serverState
          ? t("dashboard.manage.SwitchOff")
          : t("dashboard.manage.SwitchOn")}
    </p>
  );

  const reloadButton = (
    <p
      id="reload"
      className={`${style.RoundedBorders} ${switching ? style.GrayText : ""}`}
      onClick={() => reloadServer()}
    >
      {switching ? t("dashboard.manage.Wait") : t("dashboard.manage.Reload")}
    </p>
  );

  return (
    <Box title={t("dashboard.serverManage")} className={style.ControlPanel}>
      {button}
      {reloadButton}
      <p className={style.RoundedBorders}>{t("dashboard.manage.connection")}</p>
    </Box>
  );
};

Manage.propTypes = {
  serverId: PropTypes.string,
  data: PropTypes.bool,
};

export default Manage;
