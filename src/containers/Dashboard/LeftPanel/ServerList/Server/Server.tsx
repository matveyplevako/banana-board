import React from "react";
import style from "./style.css";
import { ReactLink } from "../../../../../components/ReactLink";
import { URLs } from "../../../../../urls";

import { ActiveDesktop, Desktop } from "../../../../../assets";

import PropTypes from "prop-types";

const Server = (props) => {
  const serverId = props.serverId;
  let source, classNameAddition;
  if (props.active) {
    source = ActiveDesktop;
    classNameAddition = style.Active;
  } else {
    source = Desktop;
    classNameAddition = "";
  }

  return (
    <ReactLink to={`${URLs.dashboard.url}${serverId}`}>
      <div className={`${style.Server} ${classNameAddition}`}>
        <img src={source} />
        <div>{serverId}</div>
      </div>
    </ReactLink>
  );
};

Server.propTypes = {
  serverId: PropTypes.string,
  active: PropTypes.bool,
};

export default Server;
