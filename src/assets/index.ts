import RU from "./icons/RU.svg";
import US from "./icons/US.svg";
import Notification from "./icons/notifications.svg";
import Settings from "./icons/settings.svg";
import Desktop from "./icons/Desktop.svg";
import ActiveDesktop from "./icons/ActiveDesktop.svg";
import Arrow from "./icons/keyboard_arrow_down.svg";
import AddNewAlert from "./icons/addNewAlert.svg";
import DeleteAlert from "./icons/deleteAlert.svg";
import Cross from "./icons/cross.svg";

export {
  RU,
  US,
  Notification,
  Settings,
  ActiveDesktop,
  Desktop,
  Arrow,
  AddNewAlert,
  DeleteAlert,
  Cross,
};
