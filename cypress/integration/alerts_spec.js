describe("Alerts test", () => {
    it("Visits the dashboard and test alerts", () => {
        cy.visit("/auth");

        cy.url().should("include", "/auth");

        cy.get("#login")
            .type("saymyname@yandex.ru")
            .should("have.value", "saymyname@yandex.ru");

        cy.get("#password").type("qwerty123").should("have.value", "qwerty123");

        cy.get("#signIn").click();


        cy.contains('Notifications').should('not.exist');

        cy.get('#notifications').click();

        cy.contains('Notifications').should('exist');

        cy.get(':nth-child(3) > ._-_-_-_-_-_-_-src-containers-Dashboard-Navbar-Notifications---style__Close--34I').click();
        cy.contains('No new notifications').should('not.exist');
        cy.get(':nth-child(2) > ._-_-_-_-_-_-_-src-containers-Dashboard-Navbar-Notifications---style__Close--34I').click();
        cy.contains('No new notifications').should('not.exist');
        cy.get(':nth-child(2) > ._-_-_-_-_-_-_-src-containers-Dashboard-Navbar-Notifications---style__Close--34I').click();
        cy.contains('No new notifications').should('not.exist');
        cy.get('._-_-_-_-_-_-_-src-containers-Dashboard-Navbar-Notifications---style__Close--34I').click();

        cy.contains('No new notifications').should('exist');

        cy.get('#notifications').click();

        cy.get('#notifications').click();
        cy.contains('No new notifications').should('exist');
    });
});
