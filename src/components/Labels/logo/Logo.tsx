import React from "react";

import style from "./style.css";

const Logo = () => {
  return (
    <div className={style.welcomeLogo}>
      <h1>🍌 BananaBoard</h1>
    </div>
  );
};

export { Logo };
