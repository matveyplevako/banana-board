import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Router } from "./route";
import { Provider } from "react-redux";

import { store } from "./__data__";

import "./App.css";

const App = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Router />
      </BrowserRouter>
    </Provider>
  );
};

export default App;
