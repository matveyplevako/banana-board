const pkg = require("./package");

module.exports = {
  apiPath: "stubs/api",
  webpackConfig: {
    output: {
      publicPath: `/static/${pkg.name}/${process.env.VERSION || pkg.version}/`,
    },
  },
  navigations: {
    "banana-board.auth": "/banana-board/auth",
    "banana-board": "/banana-board",
    "banana-board.dashboard": "/banana-board/dashboard/",
    "banana-board.settings": "/banana-board/settings",
    "banana-board.alerts": "/banana-board/alerts",
  },
  config: {
    "banana-board.api": "/api",
  },
};
