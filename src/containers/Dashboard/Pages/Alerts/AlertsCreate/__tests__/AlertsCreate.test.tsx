import React from "react";
import { mount } from "enzyme";

import { describe, it, expect, jest } from "@jest/globals";
import { useSelector } from "react-redux";

import AlertsCreate from "../AlertsCreate";

import { data } from "../../../../../../__data__/selectors/loadServerList";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: () => jest.fn(),
}));

describe("<AlertsCreate />", () => {
  it("AlertsCreate fill data and create", () => {
    (useSelector as any).mockImplementation((selector) => {
      switch (selector) {
      case data:
        return ["Server1", "Server2"];
      }
    });
    const wrapper = mount(<AlertsCreate />);
    expect(wrapper).toMatchSnapshot();

    wrapper.find("#create").simulate("submit");
    wrapper.update();
    expect(wrapper).toMatchSnapshot();

    wrapper
      .find("#name")
      .last()
      .simulate("change", { target: { value: "Hello" } });
    wrapper.update();

    wrapper.find("#server").simulate("click");
    wrapper.update();
    wrapper
      .find("#server")
      .find(".DropdownContent")
      .children()
      .first()
      .simulate("click");
    wrapper.update();

    wrapper.find("#resource").simulate("click");
    wrapper.update();
    wrapper
      .find("#resource")
      .find(".DropdownContent")
      .children()
      .first()
      .simulate("click");
    wrapper.update();

    wrapper.find("#conditions").simulate("click");
    wrapper.update();
    wrapper
      .find("#conditions")
      .find(".DropdownContent")
      .children()
      .first()
      .simulate("click");
    wrapper.update();

    wrapper
      .find("#value")
      .last()
      .simulate("change", { target: { value: "12" } });
    wrapper.update();

    wrapper.find("#create").simulate("submit");
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
  });

  //   it("Auth simulate login and password and then submit", () => {
  //     (useSelector as any).mockImplementation((selector) => {
  //       switch (selector) {
  //         case loading.login:
  //           return "login";
  //         case loading.password:
  //           return "password";
  //       }
  //     });
  //     const wrapper = mount(
  //       <BrowserRouter>
  //         <Auth />
  //       </BrowserRouter>
  //     );

  //     wrapper
  //       .find("#login")
  //       .last()
  //       .simulate("change", { target: { value: "Hello" } });
  //     wrapper
  //       .find("#password")
  //       .last()
  //       .simulate("change", { target: { value: "World" } });
  //     wrapper.update();
  //     expect(wrapper).toMatchSnapshot();

  //     wrapper.find("#signIn").simulate("submit");
  //     wrapper.update();
  //     expect(wrapper).toMatchSnapshot();
  //   });

  //   it("Auth login completed", () => {
  //     (useSelector as any).mockImplementation((selector) => {
  //       switch (selector) {
  //         case loading.data:
  //           return "token";
  //       }
  //     });
  //     const wrapper = mount(
  //       <BrowserRouter>
  //         <Auth />
  //       </BrowserRouter>
  //     );
  //     expect(wrapper.find("Redirect").length).toEqual(1);
  //   });
});
